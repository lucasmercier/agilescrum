# AgileScrum



## Definition of Done : 

A task is considered done only when:  
🔹 Code review is done  
🔹 Tests are successfull & Acceptance criteria met  
🔹 Style guide is fulfilled and Storybook is updated  
🔹 Non regression tests are successfully done  
🔹 Documentation is updated  
🔹 Demo with Product Owner on test environment is successfull  
